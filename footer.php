<!-- start: Footer -->
	<div id="footer">
		
		<!-- start: Container -->
		<div class="container">
			
			<!-- start: Row -->
			<div class="row">

				<!-- start: About -->
				<div class="span3">
					
					<h3>Toko Online Bama</h3>
					<p>
					Toko Bama merupakan toko yang menjual dan memasarkan sendiri Oleh-Oleh khas Lombok.
                    </p>
						
				</div>
				<!-- end: About -->

				<!-- start: Photo Stream -->
				<div class="span3">
					
					<h3>Alamat Kami</h3>
                    Jln. Pejanggik No.39 Pancor, Lombok Timur, Nusa Tenggara Barat.<br />
                    Telp/WA : 0878 2781 2748<br /></a>
                    </div>
				<!-- end: Photo Stream -->

				
			</div>
			<!-- end: Row -->	
			
		</div>
		<!-- end: Container  -->

	</div>
	<!-- end: Footer -->

	<!-- start: Copyright -->
	<div id="copyright">
	
		<!-- start: Container -->
		<div class="container">
		
			<p>
				Copyright &copy; <a href="" alt="Bootstrap Themes">Toko Online Bama &copy; 2019 ||</a> Politeknik Pos Indonesia
			</p>
	
		</div>
		<!-- end: Container  -->
		
	</div>	
	<!-- end: Copyright -->