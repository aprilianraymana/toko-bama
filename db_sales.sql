-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 06, 2019 at 06:11 AM
-- Server version: 5.5.25a
-- PHP Version: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_sales`
--

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE IF NOT EXISTS `bank` (
  `id_bank` int(2) NOT NULL AUTO_INCREMENT,
  `nama_bank` varchar(30) NOT NULL,
  `no_rek` varchar(30) NOT NULL,
  `nasabah` varchar(50) NOT NULL,
  PRIMARY KEY (`id_bank`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id_bank`, `nama_bank`, `no_rek`, `nasabah`) VALUES
(5, 'BCA', '7255047426', 'RAYMANA APRILIAN'),
(6, 'BNI', '0223874590', 'RAYMANA APRILIAN'),
(7, 'BRI', '461081010973531', 'RAYMANA APRILIAN'),
(8, 'MANDIRI', '1010005341323', 'RAYMANA APRILIAN');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE IF NOT EXISTS `cart` (
  `id_cart` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `kode` varchar(11) NOT NULL,
  `nama` varchar(11) NOT NULL,
  `size` varchar(11) NOT NULL,
  `color` varchar(10) NOT NULL,
  `harga` varchar(10) NOT NULL,
  `qty` varchar(10) NOT NULL,
  `jumlah` varchar(10) NOT NULL,
  `session` varchar(20) NOT NULL,
  PRIMARY KEY (`id_cart`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id_cart`, `tanggal`, `kode`, `nama`, `size`, `color`, `harga`, `qty`, `jumlah`, `session`) VALUES
('20180212143716', '2018-02-12 14:37:16', '32', 'tas tenun', '', '', '50000', '1', '50000', '20180129121011'),
('20180212163110', '2018-02-12 16:31:10', '32', 'tas tenun', '', '', '50000', '1', '50000', '20180129121011'),
('20180212163119', '2018-02-12 16:31:19', '32', 'tas tenun', '', '', '50000', '1', '50000', '20180129121011'),
('20190916182920', '2019-09-16 18:29:20', '19', 'Tas Motif S', '', '', '75000', '1', '75000', '20190916182844');

-- --------------------------------------------------------

--
-- Table structure for table `custom`
--

CREATE TABLE IF NOT EXISTS `custom` (
  `kode` int(10) NOT NULL AUTO_INCREMENT,
  `tanggal` varchar(30) NOT NULL,
  `kd_cus` varchar(30) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `size` varchar(20) NOT NULL,
  `color` varchar(20) NOT NULL,
  `model` varchar(100) NOT NULL,
  `gambar` varchar(40) NOT NULL,
  `harga` varchar(10) NOT NULL,
  `status` varchar(20) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `custom`
--

INSERT INTO `custom` (`kode`, `tanggal`, `kd_cus`, `nama`, `size`, `color`, `model`, `gambar`, `harga`, `status`) VALUES
(2, '0000-00-00 00:00:00', '000006', 'asik', 'M', 'black', 'short', 'kaos.jpg', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `kd_cus` varchar(20) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` text NOT NULL,
  `gambar` varchar(100) NOT NULL,
  PRIMARY KEY (`kd_cus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`kd_cus`, `nama`, `alamat`, `no_telp`, `username`, `password`, `gambar`) VALUES
('20190916182844', 'RAYMANA APRILIAN', 'Lombok', '087827812748', '1168023', '8547c0a406adbc91fe4cbff15e823e63', ''),
('20190917020046', '', 'Lombok', '0899999999999', 'ryan', '10c7ccc7a4f0aff03c915c485565b9da', ''),
('20190917020203', 'yan', 'Lombok', '082120010862', 'admin', '21232f297a57a5a743894a0e4a801fc3', ''),
('20190917020313', 'aprilian', 'Lombok', '082120010862', 'aprilian', '153b049ae7103bfcb586077a73282ae3', ''),
('20190917020721', 'joko', 'Lombok', '082120010862', 'joko', '9ba0009aa81e794e628a04b51eaf7d7f', '');

-- --------------------------------------------------------

--
-- Table structure for table `konfirmasi`
--

CREATE TABLE IF NOT EXISTS `konfirmasi` (
  `id_kon` int(6) NOT NULL AUTO_INCREMENT,
  `nopo` varchar(20) NOT NULL,
  `kd_cus` varchar(20) NOT NULL,
  `bayar_via` varchar(20) NOT NULL,
  `tanggal` datetime NOT NULL,
  `jumlah` int(10) NOT NULL,
  `bukti_transfer` varchar(50) NOT NULL,
  `status` enum('Bayar','Belum') NOT NULL,
  PRIMARY KEY (`id_kon`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `konfirmasi`
--

INSERT INTO `konfirmasi` (`id_kon`, `nopo`, `kd_cus`, `bayar_via`, `tanggal`, `jumlah`, `bukti_transfer`, `status`) VALUES
(1, '20190826093654', '20190826093654', 'BNI', '2019-08-26 09:37:40', 75000, '../admin/bukti_transfer/1164008_Dieni Hanifah.JPG', 'Belum'),
(2, '20190828031343', '20190828031343', 'Mandiri', '2019-08-28 04:40:23', 1500, '../admin/bukti_transfer/1164008_Dieni Hanifah.JPG', 'Belum'),
(3, '20190828053513', '20190828053513', 'Mandiri', '2019-08-28 05:38:07', 200000, '../admin/bukti_transfer/1164008_Dieni Hanifah.JPG', 'Belum'),
(4, '20190828053513', '20190828053513', 'Mandiri', '2019-08-28 00:00:00', 340000, '../admin/bukti_transfer/1164010_Ema Ainun Novia 3x', 'Belum'),
(5, '20190828031343', '20190828031343', 'Mandiri', '2019-09-01 10:14:22', 425000, '../admin/bukti_transfer/2760259_0.jpg', 'Bayar'),
(6, '20190917020046', '20190917020046', '0', '2019-09-18 02:39:49', 200000, '0', 'Belum'),
(7, '20190917020046', '20190917020046', '0', '2019-09-18 09:10:50', 75000, '0', 'Belum');

-- --------------------------------------------------------

--
-- Table structure for table `po`
--

CREATE TABLE IF NOT EXISTS `po` (
  `nopo` varchar(20) NOT NULL,
  `style` varchar(10) NOT NULL,
  `color` varchar(20) NOT NULL,
  `size` varchar(4) NOT NULL,
  `tanggalkirim` date NOT NULL,
  `tanggalexport` date NOT NULL,
  `status` enum('Proses','Selesai','Terkirim','') NOT NULL,
  PRIMARY KEY (`nopo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `po_terima`
--

CREATE TABLE IF NOT EXISTS `po_terima` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nopo` varchar(20) NOT NULL,
  `kd_cus` varchar(20) NOT NULL,
  `kode` int(4) NOT NULL,
  `tanggal` datetime NOT NULL,
  `style` varchar(20) NOT NULL,
  `color` varchar(10) NOT NULL,
  `size` varchar(4) NOT NULL,
  `qty` int(8) NOT NULL,
  `total` int(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `po_terima`
--

INSERT INTO `po_terima` (`id`, `nopo`, `kd_cus`, `kode`, `tanggal`, `style`, `color`, `size`, `qty`, `total`) VALUES
(1, '20190917020046', '20190917020046', 27, '2019-09-17 04:13:33', '', '', '', 1, 200000),
(2, '20190917020046', '20190917020046', 19, '2019-09-18 09:10:30', '', '', '', 1, 75000);

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE IF NOT EXISTS `produk` (
  `kode` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `harga` int(10) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `stok` int(3) NOT NULL,
  `gambar` varchar(40) NOT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=72 ;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`kode`, `nama`, `jenis`, `harga`, `keterangan`, `stok`, `gambar`) VALUES
(19, 'Tas Motif Sasak', 'tastenun', 75000, 'tas tenun warna pink dan biru gelap', 8, 'gambar_produk/2.jpg'),
(21, 'Topeng Kembar', 'kerajinantangan', 200000, 'art muka', 12, 'gambar_produk/7.jpg'),
(22, 'Miniatur Berugak', 'kerajinantangan', 170000, 'Pahatan yang berbentuk gapura', 10, 'gambar_produk/6.jpg'),
(24, 'Miniatur Bale', 'kerajinantangan', 150000, 'kerajinan tangan yang berbentuk seperti rumah', 8, 'gambar_produk/9.jpg'),
(25, 'Kain Motif Subhanale', 'kainsongket', 200000, 'kain sesek khas lombok yang bermotif subhanale', 6, 'gambar_produk/15.jpg'),
(26, 'Kain Motif Keker', 'kainsongket', 200000, 'kain sesek songket yang di buaat dengan motif keker', 13, 'gambar_produk/17.jpg'),
(27, 'Kain Motif Nanas', 'kainsongket', 200000, 'kain songket yang di sesek dengan motif yang diberi nama nanas', 4, 'gambar_produk/16.jpg'),
(28, 'Tas Motif Mbojo', 'tastenun', 50000, 'tas tenun untuk wanita', 15, 'gambar_produk/10.jpg'),
(32, 'Tas Motif Belincek', 'tastenun', 50000, 'tas tenun untuk wanita dengan motif yang diberi nama belincek', 45, 'gambar_produk/21.jpg'),
(44, 'Keraken', 'Makanan', 10000, 'Makanan yang mirip seperti dodol.', 15, 'gambar_produk/keraken.jpg'),
(55, 'Manisan Rumput Laut', 'Makanan', 25000, 'Manisan dengan bahan dasar rumput laut.', 34, 'gambar_produk/manisanrla.jpg'),
(69, 'Dodol Rumput Laut', 'Makanan', 25000, 'Dodol yang dibuat dengan rumput laut.', 45, 'gambar_produk/dodolrl.jpg'),
(71, 'Temrodok', 'makanan', 12000, 'makanan yang terbuat dari bahan dasar tepung beras', 90, 'gambar_produk/temerodok.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tmp_po_terima`
--

CREATE TABLE IF NOT EXISTS `tmp_po_terima` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nopo` varchar(10) NOT NULL,
  `kd_cus` varchar(10) NOT NULL,
  `kode` int(4) NOT NULL,
  `tanggal` date NOT NULL,
  `style` varchar(20) NOT NULL,
  `color` varchar(10) NOT NULL,
  `size` varchar(4) NOT NULL,
  `qty` int(8) NOT NULL,
  `total` int(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` text NOT NULL,
  `fullname` varchar(30) NOT NULL,
  `gambar` varchar(40) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `password`, `fullname`, `gambar`) VALUES
(6, 'admin', '21232F297A57A5A743894A0E4A801FC3', 'Raymana', 'gambar_admin/fotoRA.jpg');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
